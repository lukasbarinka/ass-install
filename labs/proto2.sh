#!/bin/bash
# Script generates animation of data progress
# Data file is named 'data'
# Create separate slides of animation
for ((i=1;i<=900;i++))
do
	# Create 1 slide of animation (NNN.png) – mv not needed
	# Data are prepared inside gnuplot using external sed filter
	echo "set terminal png;
		set output '`printf %03d $i`.png';
		set format x '';
		plot [0:100][95:105] \
		'<sed -n $i,$((100+i))p data' \
		with boxes t'$i-$((100+i))';
	" | gnuplot
	echo $i
done
# Join slides into animation
ffmpeg -i %03d.png anim.mp4

