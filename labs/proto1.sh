#!/bin/bash
# Script generates animation of data progress
# Data file is named 'data'

# Create separate slides of animation
for ((i=1;i<=1000;i++))
do
	# Create 1 slide of animation (out.png)
	(
		cat script.gp
		# Prepare data for 1 slide
		head -n "$i" data
	) | gnuplot
	echo $i
	# Rename created slide to NNN.png
	# (hope 3 digits will be enough)
	mv out.png `printf "%03d" "$i"`.png
done
# Join slides into animation
ffmpeg -i %03d.png anim.mp4

