#!/bin/bash
# Nastavi prostredi na "rozumne" hodnoty

dconf load # vypne zamykani obrazovky
dconf write /org/gnome/desktop/screensaver/lock-enabled false

# vypne audio bell (pipani v terminalu)
dconf write /org/gnome/desktop/sound/event-sounds false

# prida ceskou klavesnici
dconf write /org/gnome/desktop/input-sources/sources "[('xkb', 'us'), ('xkb', 'cz')]"
dconf write /org/gnome/desktop/input-sources/xkb-options "['grp:alt_shift_toggle']"

# prida klavesovou zkratku pro gnome-terminal (CTRL-ALT-z)
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/binding "'<Primary><Alt>z'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/command "'gnome-terminal'"
dconf write /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/name "'Terminal'"

# nastavi zobrazovani datumu v hodinach na liste
dconf write /org/gnome/desktop/interface/clock-show-date true

# zapnuti pluginu a nastaveni oblibenych aplikaci
dconf write /org/gnome/shell/enabled-extensions "['alternative-status-menu@gnome-shell-extensions.gcampax.github.com', 'Panel_Favorites@rmy.pobox.com']"
dconf write /org/gnome/shell/favorite-apps "['gnome-terminal.desktop', 'org.gnome.gedit.desktop', 'org.gnome.Nautilus.desktop', 'iceweasel.desktop']"
