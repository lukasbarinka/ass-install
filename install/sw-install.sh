echo 'deb http://www.deb-multimedia.org testing main non-free' >> /etc/apt/sources.list
aptitude -y update
aptitude -y --allow-untrusted install \
	git \
	ffmpeg \
	sharutils expect vim curl gnuplot \
	ntpdate
echo "Europe/Prague" > /etc/timezone 
dpkg-reconfigure -f noninteractive tzdata
ntpdate tik.cesnet.cz
