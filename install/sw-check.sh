#!/bin/bash

C=$(tput cols)
B=$(tput smso)
b=$(tput rmso)

declare -A version=(
	[bash]=4.3
	[grep]=2.20
	[sed]=4.0
	[mawk]=1.3
	[gawk]=4
	[wget]=1
	[curl]=7
	[vim]=7.3
	[sharutils]=4
	[expect]=5
	[gnuplot]=4.6
	[imagemagick]=6.8
	[ffmpeg]=2.6
)

declare -A check=(
	[bash]='echo "${BASH_VERSINFO[0]}.${BASH_VERSINFO[1]}"'
	[grep]='grep --version | awk "NR==1 {print \$NF}"'
	[sed]='sed --version | awk "NR==1 {print \$NF}"'
	[mawk]='mawk -W version 2>/dev/null | awk "NR==1 {print \$2}"'
	[gawk]='gawk --version | tr -d , | awk "NR==1 {print \$3}"'
	[wget]='wget --version | awk "NR==1 {print \$3}"'
	[curl]='curl --version | awk "NR==1 {print \$2}"'
	[vim]='vim --version 2>&1 | awk "NR==1 {print \$5}"'
	[sharutils]='uuencode -v v | awk "NR==1 {print \$NF}"'
	[expect]='expect -v | awk "NR==1 {print \$NF}"'	
	[gnuplot]='gnuplot --version | awk "NR==1 {print \$2}"'	
	[imagemagick]='convert --version | cut -d- -f1 | awk "NR==1 {print \$3}"'	
	[ffmpeg]='ffmpeg -version | awk "NR==1 {print \$3}"'	
)

function p {
	#local pos x y
	#stty -echo
	#echo -n $'\e[6n'
	#read -d R pos
	#stty echo
	#pos=${pos#??}
	#x=${pos#*;}
	#y=${pos%;*}
	#tput cup $y $((C-${#1}-1))
	printf '%s\n' "$B$1$b"
}

function test_version {
	local req ver x y
	req=${1//[^0-9.]/}
	ver=${2//[^0-9.]/}
	while [ -n "$req" -a -n "$ver" ]
	do
		x=${req%%.*}
		y=${ver%%.*}
		((x>y)) && return 1
		((x<y)) && return 0
		req=${req#$x}
		req=${req#.}
		ver=${ver#$y}
		ver=${ver#.}
	done
	[ -z "$req" ] && return 0
	((req==ver))
}

for sw in "${!check[@]}"
do
	printf '%s' "Checking '$sw' [${version[$sw]}] ... "
	eval v=\$\("${check[$sw]}"\) || exit 1
	printf '%s ' "$v"
	test_version "${version[$sw]}" "$v" || { p FAILED; exit 1; }
	p OK
done
